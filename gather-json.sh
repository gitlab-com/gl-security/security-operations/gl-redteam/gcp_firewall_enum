#!/bin/bash

# Script to gather all necessary json data for firewall analysis

# You can provide a regex as ARGV1 to ignore matching projects
if [ -z "$1" ]; then
    projects=$(gcloud projects list --format="get(projectId)")
else
    projects=$(gcloud projects list --format="get(projectId)" | egrep -v "$1")
fi

echo Found $(echo $projects | wc -w) projects...

for i in $projects; do
    mkdir -p ./json-data/"$i"

    echo scraping project "$i"

    # This speeds the script up dramatically in large orgs that have projects
    # with the compute API disabled
    enabled=$(gcloud services list --project "$i" 2>/dev/null| grep "Compute Engine API")
    if [ -z "$enabled" ]; then
	echo "  (compute API disabled)"
    else

        gcloud compute firewall-rules list \
            --format="json(name,allowed[].map().firewall_rule().list(),network,
                targetServiceAccounts.list(),targetTags.list(),selfLink)" \
            --filter="direction:INGRESS AND disabled:False AND
                sourceRanges.list():0.0.0.0/0 AND
                allowed[].map().firewall_rule().list():*" \
            --quiet \
            --project="$i" \
            > ./json-data/"$i"/firewall-rules.json &


        gcloud compute instances list \
            --format="json(id,name,selfLink,networkInterfaces[].accessConfigs[0].natIP,
                serviceAccounts[].email,tags.items[],networkInterfaces[].network)" \
            --filter="networkInterfaces[].accessConfigs[0].type:ONE_TO_ONE_NAT
                AND status:running" \
            --quiet \
            --project="$i" \
            > ./json-data/"$i"/compute-instances.json &

    fi
done
